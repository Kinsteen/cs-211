#include <stdio.h>

void swap(int* a, int* b);
int compare(int* a, int *b, int n);
void affiche(int* a, int n);
void swap(int* a, int* b);
void merge (int* tab, int* tmp, int left, int mid, int right, int* cnt);
int asc(int a, int b);
int desc(int a, int b);
int parity(int a, int b);
int BubbleSort(int* tab, int length,  int *fct(int a, int b));
int InsertionSort(int* tab, int length);
void MergeSort(int* tab, int* tmp, int left, int right, int* cnt);

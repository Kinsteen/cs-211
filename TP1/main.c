#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"


int main(){
	int ref[]  = {3,6,6,7,8,10,14,15,17,19,20,21,23,25,26,28,28,28,32,32,34,35,38,38,39,43,44,46,48,49,50,58,59,62,64,65,69,71,75,79,79,79,81,84,86,89,92,93,97,99};
	int tab1B[] = {14,15,92,65,35,89,79,32,38,46,26,43,38,32,79,50,28,84,19,71,69,39,93,75,10,58,20,97,49,44,59,23,07,81,64,06,28,62,8,99,86,28,03,48,25,34,21,17,06,79};
	int tab2B[] = {99,97,93,92,89,86,84,81,79,79,79,75,71,69,65,64,62,59,58,50,49,48,46,44,43,39,38,38,35,34,32,32,28,28,28,26,25,23,21,20,19,17,15,14,10,8,7,6,6,3};
	int tab3B[] = {3,6,6,7,8,10,14,15,17,19,81,21,23,25,26,28,28,28,32,32,34,35,38,38,39,43,44,46,48,49,50,58,59,62,64,65,69,71,75,79,79,79,20,84,86,89,92,93,97,99};
	int tab1I[] = {14,15,92,65,35,89,79,32,38,46,26,43,38,32,79,50,28,84,19,71,69,39,93,75,10,58,20,97,49,44,59,23,07,81,64,06,28,62,8,99,86,28,03,48,25,34,21,17,06,79};
	int tab2I[] = {99,97,93,92,89,86,84,81,79,79,79,75,71,69,65,64,62,59,58,50,49,48,46,44,43,39,38,38,35,34,32,32,28,28,28,26,25,23,21,20,19,17,15,14,10,8,7,6,6,3};
	int tab3I[] = {3,6,6,7,8,10,14,15,17,19,81,21,23,25,26,28,28,28,32,32,34,35,38,38,39,43,44,46,48,49,50,58,59,62,64,65,69,71,75,79,79,79,20,84,86,89,92,93,97,99};
	int tab1M[] = {14,15,92,65,35,89,79,32,38,46,26,43,38,32,79,50,28,84,19,71,69,39,93,75,10,58,20,97,49,44,59,23,07,81,64,06,28,62,8,99,86,28,03,48,25,34,21,17,06,79};
	int tab2M[] = {99,97,93,92,89,86,84,81,79,79,79,75,71,69,65,64,62,59,58,50,49,48,46,44,43,39,38,38,35,34,32,32,28,28,28,26,25,23,21,20,19,17,15,14,10,8,7,6,6,3};
	int tab3M[] = {3,6,6,7,8,10,14,15,17,19,81,21,23,25,26,28,28,28,32,32,34,35,38,38,39,43,44,46,48,49,50,58,59,62,64,65,69,71,75,79,79,79,20,84,86,89,92,93,97,99};
	
	int n = sizeof(ref)/sizeof(int); // n = 50

	// BUBBLE
	printf("Nb comparaisons Bubble ref : %d\n", BubbleSort(ref, n, &asc));
	printf("Nb comparaisons Bubble tab1 : %d\n", BubbleSort(tab1B, n, &asc));
	printf("Nb comparaisons Bubble tab2 : %d\n", BubbleSort(tab2B, n, &asc));
	printf("Nb comparaisons Bubble tab3 : %d\n", BubbleSort(tab3B, n, &asc));

	// INSERTION
	printf("Nb comparaisons Insert ref : %d\n", InsertionSort(ref, n));
	printf("Nb comparaisons Insert tab1 : %d\n", InsertionSort(tab1I, n));
	printf("Nb comparaisons Insert tab2 : %d\n", InsertionSort(tab2I, n));
	printf("Nb comparaisons Insert tab3 : %d\n", InsertionSort(tab3I, n));

	int cntMergeref = 0;
	int cntMerge1 = 0;
	int cntMerge2 = 0;
	int cntMerge3 = 0;

	int* tmp = malloc(n*sizeof(int));
	MergeSort(ref, tmp, 0, n-1, &cntMergeref);
	printf("Nb comparaisons Merge ref : %d\n", cntMergeref);
	free(tmp);

	int* tmp1 = malloc(n*sizeof(int));
	MergeSort(tab1M, tmp1, 0, n-1, &cntMerge1);
	printf("Nb comparaisons Merge tab1 : %d\n", cntMerge1);
	free(tmp1);

	int* tmp2 = malloc(n*sizeof(int));
	MergeSort(tab2M, tmp2, 0, n-1, &cntMerge2);
	printf("Nb comparaisons Merge tab2 : %d\n", cntMerge2);
	free(tmp2);

	int* tmp3 = malloc(n*sizeof(int));
	MergeSort(tab3M, tmp3, 0, n-1, &cntMerge3);
	printf("Nb comparaisons Merge tab3 : %d\n", cntMerge3);
	free(tmp3);

	return 0;
}

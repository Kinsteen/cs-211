#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"

void affiche(int* a, int n){
	int i;
	for (i = 0 ; i < n ; i++){
		printf("%d ", a[i]);
	}
	printf("\n\n");
}

void affRange(int* a, int start, int end) {
	int i;
	for (i = start ; i < end+1 ; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

int compare(int* a, int *b, int n){
	int i;
	for(i=0; i < n ; i++){
		if (a[i] != b[i]){
			printf("Comparison FAILED at index %d\n\n", i);
			return i;
		}
	}
	printf("Comparison OK\n\n");
	return -1;	
}

void swap(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

void merge(int* tab, int* tmp, int left, int mid, int right, int* cnt) {
	int i = left;
	int j = mid;
	int k = left;

	while (i<=mid-1 && j<=right) { // This will sort until one of the tabs are done
		if (tab[i] < tab[j])
			tmp[k++] = tab[i++];
		else
			tmp[k++] = tab[j++];

		(*cnt)++;
	}
	
	while (i <= mid-1) { // This is to complete if tabs are not the same length
		tmp[k++] = tab[i++];
		(*cnt)++;
	}

	while (j <= right) { // Same as higher
		tmp[k++] = tab[j++];
		(*cnt)++;
	}

	for (int i = left; i < k; i++) {
		tab[i] = tmp[i];
	}
}

int asc(int a, int b) { // Return 1 if needs to swap
	return a > b;
}

int desc(int a, int b) {
	return a < b;
}

int parity(int a, int b) {
	return a%2==1 && b%2==0;
}

int BubbleSort(int* tab, int length, int *fct(int a, int b)) {
	int comparisons = 0;
	int sorted = 0;

	while (!sorted)
	{
		int hasSwapped = 0;

		for (int i = 0; i < length-1; i++)
		{
			comparisons++;
			if ((*fct)(tab[i], tab[i+1]))
			{
				hasSwapped = 1;
				swap(&tab[i], &tab[i+1]);
			}
		}

		if (!hasSwapped)
		{
			sorted = 1;
		}
	}
	
	return comparisons;
}

int InsertionSort(int* tab, int length) {
	int nbOp = 0;

	for (int i = 1; i < length; i++)
	{
		int temp = tab[i];
		int j = i;

		nbOp++;
		while (j > 0 && tab[j-1] > temp)
		{
			nbOp++;
			tab[j] = tab[j-1];
			j--;
		}
		
		tab[j] = temp;
	}

	return nbOp;
}

void MergeSort(int* tab, int* tmp, int left, int right, int* cnt) {
	if(left != right) {
		int mid = (left+right)/2;

		MergeSort(tab, tmp, left, mid, cnt);
		MergeSort(tab, tmp, mid+1, right, cnt);

		merge(tab, tmp, left, mid+1, right, cnt);
	}
}
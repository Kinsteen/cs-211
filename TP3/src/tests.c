#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "fat.h"
#include "tests.h"

void green() {
  printf("\033[0;32m");
}

char* createDumb(int size) {
    char* data = malloc(sizeof(char) * size);
    for (int i = 0; i < size-1; i++) {
        data[i] = i%26+97;
    }
    data[size-1] = '\0';
    return data;
}

void testCreate() // This will test the memory
{
    initialise_fat();
    int size = BLOCSIZE/2;
    char* data = createDumb(size);

    creer_objet("Test1", 10, size, data);

    printf(" - Test Creation size < blocksize\n");
    assert(FAT[0] == 0xFFFE);
    assert(FAT[1] == 0xFFFF);
    green();
    printf("FAT is OK\n");
    printf("\033[0m");
    assert(volume[0] == 'a');
    assert(volume[size-2] >= 97 && volume[size-2] <=  122);
    assert(volume[size-1] == '\0'); // Last sentinelle
    assert(volume[size] == '\0'); // Empty volume
    green();
    printf("Volume is OK\n");
    printf("\033[0m");
    assert(strcmp(obj->nom, "Test1") == 0);
    assert(obj->taille == size);
    assert(obj->auteur == 10);
    assert(obj->index == 0);
    assert(obj->next == NULL);
    green();
    printf("Obj is OK\n");
    printf("\033[0m");

    printf(" - New object < blocksize\n");
    char* data2 = createDumb(size);
    creer_objet("Test2", 15, size, data2);

    assert(FAT[1] == 0xFFFE);
    assert(FAT[2] == 0xFFFF);
    green();
    printf("FAT is OK\n");
    printf("\033[0m");
    assert(volume[BLOCSIZE] == 'a');
    assert(volume[BLOCSIZE+size-2] >= 97 && volume[BLOCSIZE+size-2] <=  122);
    assert(volume[BLOCSIZE+size-1] == '\0'); // Last sentinelle
    assert(volume[BLOCSIZE+size] == '\0'); // Empty volume
    green();
    printf("Volume is OK\n");
    printf("\033[0m");
    assert(strcmp(obj->nom, "Test2") == 0);
    assert(obj->taille == size);
    assert(obj->auteur == 15);
    assert(obj->index == 1);
    assert(obj->next != NULL);
    green();
    printf("Obj is OK\n");
    printf("\033[0m");

    free(data);
    free(data2);
}

void testReset() {
    printf("- Test resetting memory\n");
    supprimer_tout();
    for (int i = 0; i < BLOCNUM; i++) {
        assert(FAT[i] == 0xFFFF);
    }
    for (int i = 0; i < BLOCNUM*BLOCSIZE; i++) {
        assert(volume[i] == '\0');
    }
    assert(freeblocks == BLOCNUM);
    assert(obj == NULL);
    green();
    printf("Resetting looks OK (can't search memory leak, check valgrind)\n");
    printf("\033[0m");
}

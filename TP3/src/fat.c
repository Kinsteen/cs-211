#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fat.h"
#include "tests.h"

void initialise_fat()
{
    for (int i = 0; i < BLOCNUM; i++) {
        FAT[i] = 0xFFFF;
    }

    freeblocks = BLOCNUM;
    obj = NULL;
}

int findFreeBlock()
{
    int i = 0;
    int found = 0;
    while (found == 0 && i < BLOCNUM) {
        if (FAT[i] == 0xFFFF) {
            found = 1;
        } else {
           i++;
        }
    }

    return i;
}

Objet *findLast()
{
    Objet* temp = obj;
    while (temp->next != NULL) {
        temp = temp->next;
    }

    return temp;
}

Objet *creer_objet(char *nom, unsigned short auteur,unsigned int taille, char *data)
{
    // Check if name exists or not
    Objet* temp = obj;
    int found = 0;

    while (found == 0 && temp != NULL) {
        if (strcmp(temp->nom, nom) == 0) {
            found = 1;
        }

        temp = temp->next;
    }

    if (found) {
        printf("Un objet avec le même nom a été trouvé. Impossible de construire un nouvel objet.\n");
        return NULL;
    }

    // Same name hasn't been found, continuing

    Objet* new = malloc(sizeof(Objet));
    int blockNum = findFreeBlock();
    new->index = blockNum;
    FAT[blockNum] = 0xFFFE;
    int currentDataBlockIndex = 0;

    for (int dataIndex = 0; dataIndex > 0 ? data[dataIndex-1] != '\0' : 1; dataIndex++) { // Don't forget null char at the end
        volume[currentDataBlockIndex+blockNum*BLOCSIZE] = data[dataIndex];
        currentDataBlockIndex++;

        if (currentDataBlockIndex >= BLOCSIZE) { // Block finished !
            int oldBlockNum = blockNum;
            blockNum = findFreeBlock();
            FAT[oldBlockNum] = blockNum;
            FAT[blockNum] = 0xFFFE;
            freeblocks--;

            currentDataBlockIndex = 0;
        }
    }

    freeblocks--;

    // Init new obj
    strcpy(new->nom, nom);
    new->auteur = auteur;
    new->taille = taille;
    new->next = obj;

    obj = new;

    return new;
}

Objet *rechercher_objet(char *nom)
{
    Objet* temp = obj;

    while (temp != NULL) {
        if (strcmp(temp->nom, nom) == 0) {
            return temp;
        }
        temp = temp->next;
    }

    printf("Pas trouvé\n");

    return NULL;
}

int supprimer_objet(char *nom)
{
    // On doit rechercher le block a supprimer et celui avant, on peut pas utiliser rechercher()
    Objet* temp = obj;
    int deleted = 0;

    if (temp == NULL) { // If obj is null, can't delete anything
        return -1;
    }

    if (strcmp(temp->nom, nom) == 0) { // Tete de liste
        if (temp->next != NULL) {
            obj = temp->next;

            int index = temp->index;
            while (FAT[index] != 0xFFFE) {
                int tempIndex = FAT[index];
                FAT[index] = 0xFFFF;
                index = tempIndex;
            }

            FAT[index] = 0xFFFF;
            freeblocks++;
            free(temp);
        } else {
            free(temp);
            obj = NULL;
        }
        return 0;
    }

    while (temp->next != NULL && deleted == 0) {
        if (strcmp(temp->next->nom, nom) == 0) {
            // Found block
            Objet* blockToDel = temp->next;
            Objet* blockBefore = temp;
            Objet* blockNext = temp->next->next;

            blockBefore->next = blockNext;

            // Now, deleting in FAT and in volume (optionnal ?)

            int index = blockToDel->index;

            while (FAT[index] != 0xFFFE) {
                int tempIndex = FAT[index];
                FAT[index] = 0xFFFF;
                index = tempIndex;
            }

            FAT[index] = 0xFFFF;
            freeblocks++;
            free(blockToDel);
            deleted = 1;
        }

        if (deleted == 0) { // Avoid segfault.
            temp = temp->next;
        }
    }

    return 0;
}

void supprimer_tout()
{
    for (int i = 0; i < BLOCNUM; i++) {
        FAT[i] = 0xFFFF;
    }
    freeblocks = BLOCNUM;

    Objet* temp = obj;
    while (temp != NULL){
        Objet* toDel = temp;
        temp = temp->next;
        free(toDel);
    }

    obj = NULL;
}

int lire_objet(Objet* o, char** data) // Bonus qui marche
{
    char *newData = malloc(o->taille);
    int currentBlockIndex = 0;
    int blockNum = o->index;

    for (int i = 0; i < o->taille; i++) {
        newData[i] = volume[currentBlockIndex+blockNum*BLOCSIZE];
        currentBlockIndex++;

        if (currentBlockIndex >= BLOCSIZE) {
            currentBlockIndex = 0;
            blockNum = FAT[blockNum];
        }
    }

    data = &newData;
    free(newData);

    return 0;
}

int main(void)
{
    initialise_fat();

    int dataTestSize = 25;
    char* data1 = createDumb(dataTestSize); // createDumb() crée un tableau de caractères de taille définie
    char* data2 = createDumb(dataTestSize);
    char* data3 = createDumb(dataTestSize);

    creer_objet("Test1", 10, dataTestSize, data1); // Working ?
    creer_objet("Test2", 15, dataTestSize, data2); // Working ?
    creer_objet("Test3", 20, dataTestSize, data3); // Working ?
    supprimer_objet("Test3");
    
    char** test = NULL;
    lire_objet(rechercher_objet("Test1"), test);

    free(data1);
    free(data2);
    free(data3);

    free(test);

    supprimer_tout();

    // Vous pouvez lancer des sortes de tests unitaires non finis
    //testCreate();
    //testReset();
}

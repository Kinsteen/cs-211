#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

/* Ici, on est oblig� d'utiliser la notation struct xxx,
car la structure s'auto-r�f�rence!*/
typedef struct node {
	char data ;
	struct node *link ;
} Lnode ;

/* Proc�dure d'affichage de la liste. Ne doit pas �tre modifi�e!!! */
void listeAffiche(Lnode * ptr)
{
	if ( NULL == ptr )
		printf("Liste vide!") ;
	else 
		printf("Contenu de la liste : ") ;
	while ( NULL != ptr ) 	{
		printf("%c ",ptr->data);
		ptr = ptr->link ;
		}
	printf("\n") ;
}

Lnode* findLast(Lnode* first)
{
	Lnode *temp = first;
	while (temp->link != NULL)
	{
		temp = temp->link;
	}

	return temp;
}

/* Insertion en "t�te de liste" */
void insertionTete(Lnode **ph,char item)
{
	Lnode* head = malloc(sizeof(Lnode));
	head->data = item;
	head->link = *ph;
	*ph = head;
}

/* Insertion en "queue de liste" */
void insertionQueue(Lnode **ph,char item)
{
	Lnode* last = findLast(*ph);
	Lnode* toInsert = malloc(sizeof(Lnode));
	toInsert->data = item;
	last->link = toInsert;
}

/* Suppression en "t�te de liste" */
void suppressionTete(Lnode **ph)
{
	Lnode* old = *ph;
	Lnode* newFirst = old->link;
	*ph = newFirst;
	free(old);
}

/* Suppression en "Queue" de liste" */
void suppressionQueue(Lnode **ph)
{
	Lnode *newLastElement = *ph;
	while((newLastElement->link)->link != NULL)
		newLastElement = newLastElement->link;
	
	free(newLastElement->link);
	newLastElement->link = NULL;
}

/* Programme principal. Ne doit pas �tre modifi�!!! */
int main(void) {
	Lnode *tete = NULL ;

	listeAffiche(tete) ;
	insertionTete(&tete,'a') ;
	listeAffiche(tete) ;
	insertionTete(&tete,'c') ;
	listeAffiche(tete) ;
	insertionQueue(&tete,'t') ;
	listeAffiche(tete) ;
	insertionQueue(&tete,'s') ;
	listeAffiche(tete) ;
	suppressionTete(&tete) ;
	listeAffiche(tete) ;
	suppressionTete(&tete) ;
	listeAffiche(tete) ;
	suppressionQueue(&tete) ;
	listeAffiche(tete) ;
	suppressionTete(&tete) ;
	listeAffiche(tete) ;

   return EXIT_SUCCESS;
}	

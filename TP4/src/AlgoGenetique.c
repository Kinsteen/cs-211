#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "AlgoGenetique.h" 
#include "array.h"

//#define lire(gene,i)  (i%2)?(gene[i/2]&0xF):(gene[i/2]>>4);

int lire(unsigned char *gene, int i)
{
	return (i%2)?(gene[i/2]&0xF):(gene[i/2]>>4);
}

char readCode(unsigned char *gene, int i)
{
	char code[]="+-*/";
	return code[lire(gene, i)%4];
}

void affiche(unsigned char *gene)
{
	char code[]="+-*/";
	int i=0,res;
	// the last gene is useless
	while (i<(NBGENE-1)) {
		res=lire(gene,i);
		if (i%2)
			printf("%c ",code[res%4]);
		else
			printf("%d ",res);
		i=i+1;
	}
	printf("\n");
}

void afficheArray(Array *arr)
{
	char code[]="+-*/";
	for (int i = 0; i < arr->length; i++) {
		if (i%2)
			printf("%c ", code[readArray(arr, i)%4]);
		else
			printf("%d ", readArray(arr, i));
	}
	printf("\n");
}

void convertSnakeToArray(Array *arr, Serpent *s)
{
	// the last gene is useless
	for (int i = 0; i < (NBGENE-1); i++) {
		push(arr, lire(s->gene, i));
	}
}

void calcul(Serpent *s)
{
	Array *expr = createArray();
	convertSnakeToArray(expr, s);
	int error = 0;

	// Gestion des multipli + div
	for (int i = 1; i < expr->length; i += 2) {
		int nb = readArray(expr, i);
		char code[] = "+-*/";  

		if (i%2) {
			int tempres;
			if (code[nb%4] == '*')
				tempres = readArray(expr, i-1)*readArray(expr, i+1);

			if (code[nb%4] == '/') {
				if (readArray(expr, i+1) > 0) {
					tempres = readArray(expr, i-1)/readArray(expr, i+1);
				} else {
					error = 1;
					break;
				}
			}

			if (code[nb%4] == '*' || code[nb%4] == '/') {
				writeArray(expr, i-1, tempres);

				for (int j = i+2; j < expr->length; j++) {
					writeArray(expr, j-2, readArray(expr, j));
				}

				expr->length -= 2;
				i -= 2; // Stay in place, necessary because of chain multiplication/divisions
			}
		}
	}
	
	if(error == 0) { // Si pas d'erreur de division, on continue avec les additions et soustractions
		for (int i = 1; i < expr->length; i += 2) {
			int nb = readArray(expr, i);
			char code[] = "+-*/";  

			if (i%2) { // Only + and - remaining
				//printf("%d ", i);
				int tempres;

				if (code[nb%4] == '+')
					tempres = readArray(expr, i-1)+readArray(expr, i+1);

				if (code[nb%4] == '-')
					tempres = readArray(expr, i-1)-readArray(expr, i+1);

				writeArray(expr, i-1, tempres);

				for (int j = i+2; j < expr->length; j++) {
					writeArray(expr, j-2, readArray(expr, j));
				}

				expr->length -= 2;
				i -= 2;
			}
		}
	}

	s->score = error ? MAX : abs(SEEK-readArray(expr, 0));
	
	freeArray(expr);
}


void testCalcul() 
{
	int i,expect; 
	Serpent test[]={
		{"\x67\xc6\x69\x73\x51\xff\x4a\xec\x29\xcd\xba\xab\xf2\xfb\xe3\x46\x7c\xc2\x54\xf8\x1b\xe8\xe7\x8d\x76\x5a\x2e\x63\x33\x9f\xc9\x9a",660},
		{"\x66\x32\x0d\xb7\x31\x58\xa3\x5a\x25\x5d\x05\x17\x58\xe9\x5e\xd4\xab\xb2\xcd\xc6\x9b\xb4\x54\x11\x0e\x82\x74\x41\x21\x3d\xdc\x87",735},
		{"\x70\xe9\x3e\xa1\x41\xe1\xfc\x67\x3e\x01\x7e\x97\xea\xdc\x6b\x96\x8f\x38\x5c\x2a\xec\xb0\x3b\xfb\x32\xaf\x3c\x54\xec\x18\xdb\x5c",694},
		{"\x02\x1a\xfe\x43\xfb\xfa\xaa\x3a\xfb\x29\xd1\xe6\x05\x3c\x7c\x94\x75\xd8\xbe\x61\x89\xf9\x5c\xbb\xa8\x99\x0f\x95\xb1\xeb\xf1\xb3",646},
		{"\x05\xef\xf7\x00\xe9\xa1\x3a\xe5\xca\x0b\xcb\xd0\x48\x47\x64\xbd\x1f\x23\x1e\xa8\x1c\x7b\x64\xc5\x14\x73\x5a\xc5\x5e\x4b\x79\x63",MAX},
		{"\x3b\x70\x64\x24\x11\x9e\x09\xdc\xaa\xd4\xac\xf2\x1b\x10\xaf\x3b\x33\xcd\xe3\x50\x48\x47\x15\x5c\xbb\x6f\x22\x19\xba\x9b\x7d\xf5",543},
		{"\x0b\xe1\x1a\x1c\x7f\x23\xf8\x29\xf8\xa4\x1b\x13\xb5\xca\x4e\xe8\x98\x32\x38\xe0\x79\x4d\x3d\x34\xbc\x5f\x4e\x77\xfa\xcb\x6c\x05",1302},
		{"\xac\x86\x21\x2b\xaa\x1a\x55\xa2\xbe\x70\xb5\x73\x3b\x04\x5c\xd3\x36\x94\xb3\xaf\xe2\xf0\xe4\x9e\x4f\x32\x15\x49\xfd\x82\x4e\xa9",MAX},
		{"\x08\x70\xd4\xb2\x8a\x29\x54\x48\x9a\x0a\xbc\xd5\x0e\x18\xa8\x44\xac\x5b\xf3\x8e\x4c\xd7\x2d\x9b\x09\x42\xe5\x06\xc4\x33\xaf\xcd",MAX},
		{"\xa3\x84\x7f\x2d\xad\xd4\x76\x47\xde\x32\x1c\xec\x4a\xc4\x30\xf6\x20\x23\x85\x6c\xfb\xb2\x07\x04\xf4\xec\x0b\xb9\x20\xba\x86\xc3",MAX},
		{"\x3e\x05\xf1\xec\xd9\x67\x33\xb7\x99\x50\xa3\xe3\x14\xd3\xd9\x34\xf7\x5e\xa0\xf2\x10\xa8\xf6\x05\x94\x01\xbe\xb4\xbc\x44\x78\xfa",727}
	}; 

	for(i=0;i<sizeof(test)/sizeof(Serpent);i++) {
		expect=test[i].score;
		//affiche(test[i].gene);
		calcul(&test[i]);
		if (expect != test[i].score) printf("error\n");
	}
}

void selection(Groupe *population,Groupe *parents)
{
	int didSort = 1;

	while (didSort == 1) { // Bubblesort
		didSort = 0;

		for (int i = 0; i < NBPOPULATION-1; i++) {
			if (population->membres[i].score > population->membres[i+1].score) {
				didSort = 1;
				Serpent temp = population->membres[i+1];
				population->membres[i+1] = population->membres[i];
				population->membres[i] = temp;
			}
		}
	}

	for (int i = 0; i < NBPARENTS; i++) {
		parents->membres[i] = population->membres[i];
	}

	return;
}

int evaluation(Groupe *population) 
{
	int serpentFound = 1;

	for (int i = 0; i < NBPOPULATION; i++) {
		Serpent *toTest = &(population->membres[i]);
		calcul(toTest);
		if (toTest->score == 0)
			serpentFound = 0;
	}
	
	return serpentFound;
}

void generationAleatoire(Groupe *population)
{
	srand(time(NULL));

	for (int i = 0; i < NBPOPULATION; i++) {
		Serpent serpent;

		for(int j = 0; j < NBGENE/2; j++) {
			(serpent.gene)[j] = rand()%256;
		}

		(population->membres)[i] = serpent;
	}
}

void reproduction(Groupe *population, Groupe *parents)
{
	for (int p = 0; p < NBPOPULATION; p++) {
		// Pick 2 parents
		int first = rand() % NBPARENTS;
		int second = rand() % NBPARENTS;

		while (first == second) { // Prevent parents from being the same
			first = rand() % NBPARENTS;
			second = rand() % NBPARENTS;
		}

		int charOrGene = rand() % 2;
		Serpent son;

		if(charOrGene == 0) { // Cut at char
			// Pick random point
			int point = rand() % (NBGENE/2);

			for(int i = 0; i < point; i++) {
				son.gene[i] = parents->membres[first].gene[i];
			}

			for(int i = point; i < NBGENE/2; i++) {
				son.gene[i] = parents->membres[second].gene[i];
			}
		} else { // Cut in the middle of char... Masking with aaaa aaaa & 1111 0000 | bbbb bbbb & 0000 1111
			int point = rand() % (NBGENE/2);
			int firstGene = parents->membres[first].gene[point];
			int secondGene = parents->membres[second].gene[point];
			int newGene = (firstGene & 0b11110000) | (secondGene & 0b00001111); // Mixing two genes in one

			for(int i = 0; i < point; i++) {
				son.gene[i] = parents->membres[first].gene[i];
			}

			son.gene[point] = newGene;

			for(int i = point+1; i < NBGENE/2; i++) {
				son.gene[i] = parents->membres[second].gene[i];
			}
		}

		population->membres[p] = son;
	}
}

void mutation (Groupe *population)
{
	for (int i = 0; i < NBPOPULATION; i++) {
		for (int j = 0; j < 5; j++) { // Repeat 5times because why not
			// Mutate or not ?
			int mutate = rand() % 101;

			if (mutate <= 10) { // 10% luck to mutate
				// Pick char to mutate
				int point = rand() % (NBGENE/2);
				population->membres[i].gene[point] = rand() % 256;
			}
		}
	}

}

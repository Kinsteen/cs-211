#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include "AlgoGenetique.h"
#include "array.h"

int main(int argc, char *argv[])
{
	testCalcul();

	FILE* file = fopen("data.txt", "w");

	int opt,nbgeneration=0;
 	Groupe population,parents;

	// les valeurs par defaut.
	population.nombre = NBPOPULATION;
	parents.nombre = NBPARENTS;
	int genNumber = NBGENERATION;

 	while ((opt = getopt(argc, argv, "p:P:g:")) != -1) {
               switch (opt) {
               case 'p':
                   population.nombre = atoi(optarg);
                   break;
               case 'P':
                   parents.nombre = atoi(optarg);
                   break;
				case 'g':
					genNumber = atoi(optarg);
					break;
               default: // ?
                   fprintf(stderr, "Usage: %s [-p nbpopulation] [-P nbparents] [-g nbgeneration] \n",argv[0]);
                   exit(EXIT_FAILURE);
               }
        }
	// test et allocation mémoire.
	if (parents.nombre > population.nombre) exit(EXIT_FAILURE);

	if ((population.membres=malloc(sizeof(Serpent)*population.nombre)) == NULL ) exit(EXIT_FAILURE);
	if ((parents.membres=malloc(sizeof(Serpent)*parents.nombre)) == NULL ) exit(EXIT_FAILURE);

	int successGen = 0;
	// creation de la premiere génération
	generationAleatoire(&population);
	nbgeneration++;
	while (evaluation(&population)) {
		successGen += evaluation(&population) ? 0 : 1;
		printf("Generation %d...\n", nbgeneration);
		selection(&population,&parents);
		fprintf(file, "%d\n", population.membres[0].score);
		affiche(population.membres[0].gene);
		printf("%d\n", population.membres[0].score);
		affiche(population.membres[1].gene);
		printf("%d\n", population.membres[1].score);
		affiche(population.membres[2].gene);
		printf("%d\n", population.membres[2].score);
		reproduction(&population,&parents);
		mutation(&population);
		nbgeneration++;
	}

	selection(&population,&parents);
	fprintf(file, "%d\n", population.membres[0].score);

	printf("%d successful gen\n", successGen);

	free(population.membres);
	free(parents.membres);
	fclose(file);
}
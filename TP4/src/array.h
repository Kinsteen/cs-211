#ifndef ARRAY_H
#define ARRAY_H

typedef struct element
{
    int data;
    struct element *next;
} Element;

typedef struct array
{
    int length;
    Element *first;
} Array;

Array *createArray();

int readArray(Array *arr, int index);
void writeArray(Array *arr, int index, int data);

Element* findLast(Array *arr);

void push(Array* arr, int data);
int slice(Array* arr);

void freeArray(Array *arr);

#endif
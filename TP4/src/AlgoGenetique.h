#ifndef NBPOPULATION
#define NBPOPULATION 200
#endif

#ifndef NBPARENTS
#define NBPARENTS 5
#endif

#ifndef NBGENERATION
#define NBGENERATION 500
#endif

#define MAX 2000000
#define SEEK 666

#ifndef NBGENE
#define NBGENE 64
#endif

typedef struct serpent {
	unsigned char gene[NBGENE/2];
	int score;
} Serpent;

typedef struct groupe {
	Serpent *membres;
	int nombre;
} Groupe;

void affiche(unsigned char *gene);
void calcul(Serpent *g);
void testCalcul();
void selection(Groupe *population,Groupe *parents);
int evaluation(Groupe *population);
void generationAleatoire(Groupe *population);
void reproduction(Groupe *population,Groupe *parents);
void mutation (Groupe *population);

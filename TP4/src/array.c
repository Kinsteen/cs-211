#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "array.h"

Array *createArray()
{
    Array *array = malloc(sizeof(Array));

    if (array == NULL) {
        fprintf(stderr, "Can't allocate memory for array\n");
        exit(EXIT_FAILURE);
    }

    array->length = 0;
    array->first = NULL;

    return array;
}

int readArray(Array *arr, int index)
{
    if (index >= arr->length) {
        return -1;
    }

    Element *temp = arr->first;
    for(int i = 0; i < index; i++) {
        temp = temp->next;
    }

    return temp->data;
}

void writeArray(Array *arr, int index, int data)
{
    if (index >= arr->length) {
        return;
    }

    int i = 0;
    Element* temp = arr->first;
    while (i < index) {
        temp = temp->next;
        i++;
    }

    temp->data = data;
}

Element* findLast(Array *arr)
{
    Element *element = arr->first;

    if (element == NULL) {
        return NULL;
    }

    while (element->next != NULL) {
        element = element->next;
    }

    return element;
}

void push(Array* arr, int data)
{
    Element *lastElement = findLast(arr);

    if (lastElement == NULL) {
        arr->first = malloc(sizeof(Element));
        arr->length++;
        arr->first->data = data;
        arr->first->next = NULL;

        return;
    }

    lastElement->next = malloc(sizeof(Element));
    Element *newElement = lastElement->next;
    newElement->data = data;
    newElement->next = NULL;
    arr->length++;
}

int slice(Array* arr)
{
    Element *element = arr->first;

    if (element == NULL) {
        return -1;
    }

    if (element->next == NULL) {
        free(element);
        arr->first = NULL;
        arr->length = 0;
    }

    while (element->next->next != NULL) {
        element = element->next;
    }

    free(element->next);
    element->next = NULL;
    arr->length--;

    return 0;
}

void freeArray(Array *arr)
{
    Element* temp = arr->first;

    while (temp != NULL) {
        Element *toDel = temp;
        temp = temp->next;
        free(toDel);
    }

    free(arr);
}

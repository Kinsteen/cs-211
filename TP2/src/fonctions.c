#include <stdio.h>
#include <stdlib.h>

#include "fonctions.h"
#include "bitmap.h"

void printbinchar(char character)
{
    int i;
    for (i = 7; i >= 0; i--) {
        printf("%d", character>>i & 1);
    }

    printf(" ");
}

FILE* openFile(char* name, char* mode)
{
    FILE* fd;
    fd = fopen(name, mode);

    if (fd == NULL) {
        fprintf(stderr, "Erreur lors de l'ouverture du fichier : %s\n", name);
        perror("");
        exit(-1);
    }

    return fd;
}

void closeFile(FILE* file)
{
    if (fclose(file) == EOF) {
        fprintf(stderr, "Erreur lors de la fermeture du fichier\n");
        perror("");
        exit(-1);
    }
}

unsigned char* readBitmap(FILE* file, FileHeader* fileHeader, ImageHeader* imageHeader) // Pass data block size as pointer
{
    unsigned char* bitmapImage;

    fread(fileHeader, sizeof(FileHeader), 1, file); // Read bmp file into structure
    // This will directly put data from "file" into the memory at fileHeader pointer
    // Because structure sizes are correctly defined, everything will match correctly

	printf("FileHeader\n");
	printf("Signature: %d\n", fileHeader->signature);
	printf("Taille fichier: %d\n", fileHeader->tailleFichier);
	printf("Réservé: %d\n", fileHeader->reserve);
	printf("Décalage: %d\n", fileHeader->offset);

    // Check magic number to match BMP file
    if (fileHeader->signature != 0x4D42) {
        printf("NOT A BITMAP\n");
        return NULL;
    }

    fread(imageHeader, sizeof(ImageHeader), 1, file);
    // Same principle as "fileHeader"

	printf("ImageHeader\n");
	printf("Taille entete: %d\n", imageHeader->tailleEntete);
	printf("Largeur: %d\n", imageHeader->largeur);
	printf("Hauteur: %d\n", imageHeader->hauteur);
	printf("Plan: %d\n", imageHeader->plan);
	printf("Profondeur: %d\n", imageHeader->profondeur);
	printf("Compression: %d\n", imageHeader->compression);
	printf("Taille image: %d\n", imageHeader->tailleImage);
	printf("Resolution Horizontale: %d\n", imageHeader->resolutionHorizontale);
	printf("Resolution Verticale: %d\n", imageHeader->resolutionVerticale);
	printf("Nombre couleurs: %d\n", imageHeader->nombreCouleurs);
	printf("Nombre couleurs importantes: %d\n", imageHeader->nombreCouleursImportantes);
	printf("\n------\n\n");

    fseek(file, fileHeader->offset, SEEK_SET); // Move cursor to data offset
    // SEEK_SET means "move cursor of file to offset 'fileHeader.offset' starting from START"

    bitmapImage = (unsigned char*) malloc(imageHeader->tailleImage); // Allocate memory to save image data as char

    // Check if memory was allocated
    if (!bitmapImage) {
        printf("CAN'T ALLOCATE MEMORY\n");
        free(bitmapImage);
        return NULL;
    }

    fread(bitmapImage, imageHeader->tailleImage, 1, file);

    //make sure bitmap image data was read
    if (bitmapImage == NULL) {
        printf("BITMAP NULL\n");
        free(bitmapImage);
        return NULL;
    }

    return bitmapImage;
}

int decode(FILE* trans, FILE* output)
{
    FileHeader fileHeader;
    ImageHeader imageHeader;

    unsigned char* bitmapImage = readBitmap(trans, &fileHeader, &imageHeader); // Passing headers so we can access them later

    if (bitmapImage == NULL) {
        printf("Cannot continue. Something went wrong reading BMP file.\n");
        return -1;
    }

    int byteCounter = 0;
    unsigned char byte = 0; // Can have surprises when not initialized to 0
    for (int i = 0; i < imageHeader.tailleImage; i++) // Cycle through every byte of color data
    {
        char bit = bitmapImage[i] & 1; // This will get the least significant bit

        if (byteCounter < 8) {
            byte = byte << 1;
            byte += bit;
            byteCounter++;
        } else {
            fputc(byte, output);
            byte = bit;
            byteCounter = 1;
        }
    }

    free(bitmapImage);
    return 0;
}

int encode(FILE* source, FILE* trans, FILE* output)
{
    FileHeader fileHeader;
    ImageHeader imageHeader;
    unsigned char* bitmap;

    bitmap = readBitmap(trans, &fileHeader, &imageHeader); // This is a hidden malloc, take care

    fwrite(&fileHeader, sizeof(FileHeader), 1, output);
    fwrite(&imageHeader, sizeof(ImageHeader), 1, output);

    fseek(output, fileHeader.offset, SEEK_SET);

    // This will get how many bytes are in the source file
    fseek(source, 0, SEEK_END);
    long fsize = ftell(source);
    fseek(source, 0, SEEK_SET);

    unsigned char* sourceData = malloc(fsize + 1);
    fread(sourceData, fsize, 1, source);
	int max = 0;

    for (long j = 0; j < fsize; j++) {
        unsigned char sourceByte = sourceData[j];
        for (int n = 7; n >= 0; n--) { // To read bit in good order ; most significant -> less significant
            int sourceBit = sourceByte >> n & 1;
            unsigned char transByte = bitmap[j*8+(7-n)];
			max = j*8+(7-n);

            if (sourceBit) {
                transByte = transByte | 0b00000001;
            } else {
                transByte = transByte & 0b11111110;
            }
            
            fputc(transByte, output);
        }
    }

	for (int i = max+1; i < imageHeader.tailleImage; i++) { // This is to complete bmp file if source is smaller than transporteur
		fputc(bitmap[i], output);
	}

    free(bitmap);
    free(sourceData);

    return 0;
}

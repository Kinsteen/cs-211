#include <stdio.h>
#include <ctype.h>

#include "fonctions.h"

int main(void)
{
	/**************

	PARTIE 1

	**************/
	FILE* transPart1 = openFile("data/transporteur.txt", "r");
	FILE* outputPart1 = openFile("DroitsHomme.txt", "w");

	char c;
	int byteCounter = 0;
	char byte = 0; // Char is one byte long, exactly what we want

	while ((c=fgetc(transPart1)) != EOF) { // c will be assigned to a char in file, and while it isn't end of file, retrieve char one by one
		if (isalpha(c)) {
			int bit = isupper(c) ? 1 : 0; // isupper() does not return 1 or 0

			if (byteCounter < 8) {
				byte = byte << 1; // Shift left by one, then add 1
				byte += bit;
				byteCounter++; // Up the counter of correct bit in byte
				/*
				Ex : byte = 00100100
				aftr:byte = 01001001
				*/
			} else {
				fputc(byte, outputPart1); // Write complete byte in file
				byte = bit; // Don't forget current bit
				byteCounter = 1; // Counter is 1 because we just entered a bit
			}
		}
	}

	closeFile(transPart1);
	closeFile(outputPart1);

	/**************

	PARTIE 2

	**************/

    // Encode image
    FILE* source = openFile("sourceDecode2.jpg", "rb");
    FILE* trans = openFile("data/originel.bmp", "rb"); // Open bmp as binary
    FILE* output = openFile("output.bmp", "w");

    encode(source, trans, output);

	closeFile(source);
    closeFile(trans);
    closeFile(output);

	// Decode encoded image to test
    FILE* trans2 = openFile("data/transporteur.bmp", "rb"); // L'homme caché est Jon Postel
    FILE* output2 = openFile("sourceDecode2.jpg", "w");

    if (decode(trans2, output2) == -1) {
        printf("Something went wrong !\n");

        closeFile(trans2);
        closeFile(output2);
        return -1;
    } else {
        printf("Decoded !\n");
    }

    closeFile(trans2);
    closeFile(output2);

    return 0;
}

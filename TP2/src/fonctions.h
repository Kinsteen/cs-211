#ifndef FONCTIONS_H
#define FONCTIONS_H

#include "bitmap.h"

void printbinchar(char character);

FILE* openFile(char* name, char* mode);
void closeFile(FILE* file);

unsigned char* readBitmap(FILE* file, FileHeader* fileHeader, ImageHeader* imageHeader);

int decode(FILE* trans, FILE* output);
int encode(FILE* source, FILE* trans, FILE* output);

#endif